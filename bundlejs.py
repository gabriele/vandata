import glob
template = """
"{filename}" :  {content},
"""


with open("datafiles.js", 'w') as fd:
    fd.write("var datafiles = {")
    for json_file in glob.glob("*.json"):
         towrite = template.format(filename=json_file, 
                                   content=open(json_file).read())
         fd.write(towrite)
    fd.write("}")

